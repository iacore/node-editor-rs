pub type EBox = euclid::default::Box2D<i32>;
pub type ESize = euclid::default::Size2D<i32>;
pub type EPoint = euclid::default::Point2D<i32>;
pub type EVector = euclid::default::Vector2D<i32>;
