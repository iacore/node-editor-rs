mod el_bracket;
mod math;
mod visual;

pub use el_bracket::*;
pub use math::*;
pub use visual::*;

#[derive(Default)]
pub struct Document {
    pub elements: Vec<Element>,
}

impl Document {
    /// ## Returns
    /// None: success
    /// Some(_): failure
    #[must_use]
    pub fn try_insert(&mut self, el: Element) -> Option<Element> {
        match self.elements.binary_search_by_key(&el.id(), |el2| el2.id()) {
            Ok(_) => Some(el),
            Err(index) => {
                self.elements.insert(index, el);
                None
            }
        }
    }

    pub fn get_element_mut(&mut self, el_id: ElementId) -> Option<&mut Element> {
        match self.elements.binary_search_by_key(&el_id, |el2| el2.id()) {
            Ok(index) => Some(unsafe { self.elements.get_unchecked_mut(index) }),
            Err(_) => None,
        }
    }

    pub(crate) fn first_hovered(&self, pos: EPoint) -> Option<(ElementId, HotSpotId)> {
        for el in self.elements.iter().rev() {
            for hotspot in el.inner.iter_hotspots() {
                if hotspot.aabb.contains(pos) {
                    return Some((el.id(), hotspot.id));
                }
            }
        }
        None
    }
}

#[derive(derivative::Derivative, Clone, Copy)]
#[derivative(PartialEq, Eq, PartialOrd, Ord)]
pub struct ElementId(pub u16);

pub struct Element {
    id: ElementId,
    pub inner: Box<dyn IElement>,
}

impl Element {
    pub fn new_with_rng(rng: &mut impl rand::Rng, inner: impl IElement + 'static) -> Self {
        Self {
            id: ElementId(rng.gen()),
            inner: Box::new(inner),
        }
    }
    pub fn id(&self) -> ElementId {
        self.id
    }
}

/// what a algograph element should be like
pub trait IElement {
    fn iter_hotspots(&self) -> Box<dyn Iterator<Item = HotSpot>>;
    fn move_by(&mut self, hotspot_id: HotSpotId, delta_pos: EVector);
    fn iter_gfx(&self, state: ElementState) -> Box<dyn Iterator<Item = Visual>>;
}

#[derive(derivative::Derivative, Clone, Copy)]
#[derivative(PartialEq, Eq, PartialOrd, Ord)]
pub struct HotSpotId(pub u8);

pub struct HotSpot {
    pub id: HotSpotId,
    pub aabb: EBox,
}
