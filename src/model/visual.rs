use super::*;
use femtovg::Color;

#[derive(Clone, Copy)]
pub enum ElementState {
    Normal,
    Hovered(HotSpotId),
    Dragged(HotSpotId),
    // Active,
}

pub enum Visual {
    Rectangle{aabb: EBox, color: Color},
}

impl Visual {
    pub fn draw_on_canvas<T: femtovg::Renderer>(
        self,
        canvas: &mut femtovg::Canvas<T>,
    ) {
        match self {
            Visual::Rectangle{aabb, color} => {
                // todo: better graphics
                canvas.clear_rect(
                    aabb.min.x as u32,
                    aabb.min.y as u32,
                    aabb.width() as u32,
                    aabb.height() as u32,
                    color,
                );
            }
        }
    }
}
