use femtovg::Color;

use crate::opencolor;

use super::*;

pub struct Bracket {
    pub h0: EPoint,
    pub h1: EPoint,
}

const BRACKET_SIZE: ESize = ESize::new(40, 40);

fn bracket_aabbs(b: &Bracket) -> [EBox; 2] {
    [
        EBox::new(b.h0 - BRACKET_SIZE / 2, b.h0 + BRACKET_SIZE / 2),
        EBox::new(b.h1 - BRACKET_SIZE / 2, b.h1 + BRACKET_SIZE / 2),
    ]
}

impl IElement for Bracket {
    fn iter_hotspots(&self) -> Box<dyn Iterator<Item = HotSpot>> {
        let aabbs = bracket_aabbs(self);
        Box::new(
            [
                HotSpot {
                    id: HotSpotId(0),
                    aabb: aabbs[0],
                },
                HotSpot {
                    id: HotSpotId(1),
                    aabb: aabbs[1],
                },
            ]
            .into_iter(),
        )
    }

    fn move_by(&mut self, hotspot_id: HotSpotId, dp: EVector) {
        match hotspot_id.0 {
            0 => {
                self.h0 += dp;
                self.h1.y = self.h0.y;
            }
            1 => {
                self.h1 += dp;
                self.h0.y = self.h1.y;
            }
            x => panic!("[Bracket::move_by]  invalid hotspot id {x}"),
        }
    }

    fn iter_gfx(&self, state: ElementState) -> Box<dyn Iterator<Item = Visual>> {
        fn color(i: u8, state: ElementState) -> Color {
            match state {
                ElementState::Hovered(hsid) if hsid.0 == i => *opencolor::BLUE_3,
                ElementState::Dragged(hsid) if hsid.0 == i => *opencolor::BLUE_5,
                _ => *opencolor::BLUE_5,
                // _ => Color::rgb(255, 0, 255),
            }
        }

        Box::new(
            bracket_aabbs(self)
                .into_iter()
                .enumerate()
                .map(move |(i, aabb)| Visual::Rectangle {
                    aabb,
                    color: color(i as u8, state),
                }),
        )
    }
}
