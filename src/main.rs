mod model;
mod opencolor;

use crate::{
    app::AppConfig,
    model::{Bracket, Document, Element},
};

mod app;

fn main() -> anyhow::Result<()> {
    let mut app = app::Application::init(AppConfig {
        win_size: (600, 600),
    })?;

    let mut document = Document::default();
    assert!(document
        .try_insert(Element::new_with_rng(
            &mut app.rng,
            Bracket {
                h0: (200, 100).into(),
                h1: (400, 100).into(),
            }
        ))
        .is_none());
    app.state = app::ApplicationState::Editing(document, Default::default());

    app.run_loop()
}
