use crate::model::*;

use glfw::Context;
use rand::rngs::OsRng;

#[derive(Default)]
pub struct AuxState {
    dragging: Option<(ElementId, HotSpotId)>,
    /// [prev, now]
    mouse_pos: [EPoint; 2],
}
impl AuxState {
    fn set_mouse_pos(&mut self, pos: EPoint) {
        self.mouse_pos[0] = self.mouse_pos[1];
        self.mouse_pos[1] = pos;
    }

    fn delta_pos(&self) -> EVector {
        self.mouse_pos[1] - self.mouse_pos[0]
    }
}

#[derive(Default)]
pub enum ApplicationState {
    #[default]
    Dummy,
    Editing(Document, AuxState),
}

pub struct Application {
    pub state: ApplicationState,
    pub rng: Box<dyn rand::RngCore>,
    pub(crate) glfw: glfw::Glfw,
    pub(crate) window: glfw::Window,
    pub(crate) events: std::sync::mpsc::Receiver<(f64, glfw::WindowEvent)>,
    pub(crate) canvas: femtovg::Canvas<femtovg::renderer::OpenGl>,
}

pub struct AppConfig {
    pub(crate) win_size: (u32, u32),
}

impl Application {
    pub fn init(config: AppConfig) -> anyhow::Result<Self> {
        let mut glfw = glfw::init(glfw::FAIL_ON_ERRORS)?;
        glfw.window_hint(glfw::WindowHint::Resizable(false));
        let (mut window, events) = glfw
            .create_window(
                config.win_size.0,
                config.win_size.1,
                "Hello this is window",
                glfw::WindowMode::Windowed,
            )
            .expect("Failed to create GLFW window.");
        window.set_key_polling(true);
        window.make_current();

        let renderer = unsafe {
            femtovg::renderer::OpenGl::new_from_function(|s| window.get_proc_address(s))
        }?;
        let canvas = femtovg::Canvas::new(renderer)?;

        Ok(Self {
            glfw,
            window,
            events,
            canvas,
            state: Default::default(),
            rng: Box::new(OsRng::default()),
        })
    }

    pub fn run_loop(&mut self) -> anyhow::Result<()> {
        while !self.window.should_close() {
            self.glfw.poll_events();
            for (_, event) in glfw::flush_messages(&self.events) {
                handle_window_event(&mut self.window, event);
            }
            self.update_input();
            self.draw();
            self.window.swap_buffers();
        }
        Ok(())
    }

    pub(crate) fn draw(&mut self) {
        match &mut self.state {
            ApplicationState::Dummy => {}
            ApplicationState::Editing(document, aux) => {
                let size = self.window.get_framebuffer_size();
                self.canvas.set_size(size.0 as u32, size.1 as u32, 1.0);
                self.canvas.reset();
                self.canvas.clear_rect(
                    0,
                    0,
                    size.0 as u32,
                    size.1 as u32,
                    femtovg::Color::rgbf(0.3, 0.3, 0.32),
                );

                let pos = self.window.get_cursor_pos();
                let pos: EPoint = (pos.0 as i32, pos.1 as i32).into();
                let first_hovered = document.first_hovered(pos);

                for el in document.elements.iter() {
                    let mut el_state = ElementState::Normal;
                    match first_hovered {
                        Some((id, hsid)) if id == el.id() => {
                            el_state = ElementState::Hovered(hsid);
                        }
                        _ => {}
                    }
                    match aux.dragging {
                        Some((id, hsid)) if id == el.id() => {
                            el_state = ElementState::Dragged(hsid);
                        }
                        _ => (),
                    }
                    for gfx_el in el.inner.iter_gfx(el_state) {
                        gfx_el.draw_on_canvas(&mut self.canvas);
                    }
                }
                self.canvas.flush();
            }
        }
    }

    fn update_input(&mut self) {
        match &mut self.state {
            ApplicationState::Dummy => {}
            ApplicationState::Editing(document, aux) => {
                let pos = self.window.get_cursor_pos();
                let pos: EPoint = (pos.0 as i32, pos.1 as i32).into();
                aux.set_mouse_pos(pos);
                if let glfw::Action::Press =
                    self.window.get_mouse_button(glfw::MouseButton::Button1)
                {
                    if let Some(x) = document.first_hovered(pos) {
                        aux.dragging = Some(x);
                    }
                }
                if let Some((el_id, hotspot_id)) = aux.dragging {
                    if let Some(el) = document.get_element_mut(el_id) {
                        el.inner.move_by(hotspot_id, aux.delta_pos());
                    }
                }
                if let glfw::Action::Release =
                    self.window.get_mouse_button(glfw::MouseButton::Button1)
                {
                    aux.dragging = None
                }
            }
        }
    }
}

fn handle_window_event(window: &mut glfw::Window, event: glfw::WindowEvent) {
    use glfw::*;
    match event {
        glfw::WindowEvent::Key(Key::Escape, _, Action::Press, _) => window.set_should_close(true),
        _ => {}
    }
}
